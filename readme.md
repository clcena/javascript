[Javascript Snippets for SublimeText2](http://cristiancena.github.com/JavaScript/)
=========
### Autocompletado javascript para SublimeText2 
Modificaciones del paquete "javascript" por defecto de Sublime Text 2 - Beta, Build: 2181.

* * *

Fuentes:
- [Ayuda de Sublime Text](http://sublimetext.info/docs/es/index.html) - [Extensibilidad](http://sublimetext.info/docs/es/extensibility/snippets.html)
- [JavaScript Bible](http://www.amazon.com/JavaScript-Bible-Danny-Goodman/dp/0470069163/) de Danny Goodman, sexta edición, apéndice A: "Javascript and Browser Objects Quick Reference" [PDF](http://www.dannyg.com/dl/JSB6RefBooklet.pdf)
- [Centro de desarrolladores de Mozilla [MDN]](https://developer.mozilla.org/en/JavaScript/Reference): Referencia de JavaScript. 
- [w3schools](http://www.w3schools.com/jsref/) JavaScript and HTML DOM Reference
- [Centro de desarrolladores de Microsoft [MSDN]](http://msdn.microsoft.com/en-us/library/d1et7k7c(v=VS.94).aspx) - JavaScript Language Reference
- [Standard ECMA-262 5.1](http://www.ecma-international.org/publications/standards/Ecma-262.htm) - EECMAScript Language Specification
- [Javascript Web Apis](http://ecma-international.org/ecma-262/5.1/) - Especificaciones W3C 
